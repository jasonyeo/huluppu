.. huluppu documentation master file, created by
   sphinx-quickstart on Wed May 23 11:51:26 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to huluppu's documentation!
===================================

`huluppu` lets you search, visualize, analyze, and share findings on the evolution of research networks
Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

